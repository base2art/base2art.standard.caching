﻿namespace Base2art.Standard.Caching.Features
{
    using System;
    using System.Threading.Tasks;
    using Base2art.Caching;
    using FluentAssertions;
    using Xunit;

    public class CacheFeature
    {
        private void AreEqual<T>(T actual, T expected)
        {
            actual.Should().Be(expected);
        }

        private class ValueContainer<T>
        {
            public T Value { get; set; }
        }

        [Fact]
        public void ShouldGetItem_AsApi()
        {
            var i = new ValueContainer<int>();
            var hold = TimeSpan.FromMilliseconds(45);
            var cache = new InMemoryCacheProvider().GetSystemCache();
            Func<ValueContainer<int>> method = () =>
            {
                i.Value += 1;
                return i;
            };
            var rez = cache.As<ValueContainer<int>>().TryGet(method);
            this.AreEqual(rez.Value, 1);
            rez = cache.As<ValueContainer<int>>().TryGet(method);
            this.AreEqual(rez.Value, 1);
            cache.Clear();
            rez = cache.As<ValueContainer<int>>().TryGet(method);
            this.AreEqual(rez.Value, 2);
            rez = cache.As<ValueContainer<int>>().TryGet(method);
            this.AreEqual(rez.Value, 2);
        }

        [Fact]
        public async void ShouldGetItem_DynamicCache()
        {
            var i = new ValueContainer<int>();
            var hold = TimeSpan.FromMilliseconds(45);
            var cache = new InMemoryCacheProvider().CreateCache<string, ValueContainer<int>>(hold);
            Func<ValueContainer<int>> method = () =>
            {
                i.Value += 1;
                return i;
            };
            var rez = cache.TryGet(method);
            this.AreEqual(rez.Value, 1);
            rez = cache.TryGet(method);
            this.AreEqual(rez.Value, 1);
            cache.Clear();
            rez = cache.TryGet(method);
            this.AreEqual(rez.Value, 2);
            rez = cache.TryGet(method);
            this.AreEqual(rez.Value, 2);

            await Task.Delay(hold * 2);
            rez = cache.TryGet(method);
            this.AreEqual(rez.Value, 3);
        }

        [Fact]
        public void ShouldGetItem_SystemCache()
        {
            var i = 0;
            var cache = new InMemoryCacheProvider();
            Func<int> method = () =>
            {
                i += 1;
                return i;
            };
            var rez = cache.GetSystemCache().TryGet(method);
            this.AreEqual(rez, 1);
            rez = cache.GetSystemCache().TryGet(method);
            this.AreEqual(rez, 1);
            cache.GetSystemCache().Clear();
            rez = cache.GetSystemCache().TryGet(method);
            this.AreEqual(rez, 2);
        }
    }
}