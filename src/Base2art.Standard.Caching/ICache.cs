﻿namespace Base2art.Caching
{
    using System;

    public interface ICache<in TKey, TValue>
    {
        TValue GetItem(TKey key);

        bool HasItem(TKey key);

        void Upsert(TKey key, TValue value);

        void Upsert(TKey key, TValue value, TimeSpan keepForDuration);

        void Clear();
    }
}