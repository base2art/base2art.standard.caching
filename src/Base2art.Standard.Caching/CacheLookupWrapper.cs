﻿namespace Base2art.Caching
{
    using System;
    using System.Threading.Tasks;

    public class CacheLookupWrapper<T>
    {
        private readonly ICache<string, object> cache;

        public CacheLookupWrapper(ICache<string, object> cache) => this.cache = cache;

        public TResult TryGet<TResult>(Func<TResult> getter) where TResult : class => this.cache.TryGet(getter);

        public TResult TryGet<TIn1, TResult>(TIn1 in1, Func<TIn1, TResult> getter) where TResult : class => this.cache.TryGet(in1, getter);

        public async Task<TResult> TryGetAsync<TResult>(Func<Task<TResult>> getter) where TResult : class =>
            await this.cache.TryGetAsync<object, TResult>(getter);

        public async Task<TResult> TryGetAsync<TIn1, TResult>(TIn1 in1, Func<TIn1, Task<TResult>> getter) where TResult : class =>
            await this.cache.TryGetAsync(in1, getter);
    }
}