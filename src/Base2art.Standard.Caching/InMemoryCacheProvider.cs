﻿namespace Base2art.Caching
{
    using System;
    using System.Collections.Generic;

    public class InMemoryCacheProvider : ICacheProvider
    {
        private readonly Dictionary<string, Tuple<DateTime, object>> systemCache = new Dictionary<string, Tuple<DateTime, object>>();

        public ICache<string, object> GetSystemCache() => new InMemoryCache<string, object>(this.systemCache, TimeSpan.FromMinutes(10));

        public ICache<TKey, TValue> CreateCache<TKey, TValue>(TimeSpan defaultKeepForDuration) =>
            new InMemoryCache<TKey, TValue>(new Dictionary<TKey, Tuple<DateTime, TValue>>(), defaultKeepForDuration);

        private class InMemoryCache<TKey, TValue> : ICache<TKey, TValue>
        {
            private readonly Dictionary<TKey, Tuple<DateTime, TValue>> backingCache;

            private readonly TimeSpan defaultKeepForDuration;

            public InMemoryCache(Dictionary<TKey, Tuple<DateTime, TValue>> backingCache, TimeSpan defaultKeepForDuration)
            {
                this.defaultKeepForDuration = defaultKeepForDuration;
                this.backingCache = backingCache;
            }

            public TValue GetItem(TKey key) => this.HasItem(key) ? this.backingCache[key].Item2 : default(TValue);

            public bool HasItem(TKey key)
            {
                Tuple<DateTime, TValue> value;
                if (!this.backingCache.TryGetValue(key, out value))
                {
                    return false;
                }

                return value.Item1 > DateTime.UtcNow;
            }

            public void Upsert(TKey key, TValue value)
            {
                this.Upsert(key, value, this.defaultKeepForDuration);
            }

            public void Upsert(TKey key, TValue value, TimeSpan keepForDuration)
            {
                this.backingCache[key] = Tuple.Create(DateTime.UtcNow.Add(this.defaultKeepForDuration), value);
            }

            public void Clear()
            {
                this.backingCache.Clear();
            }
        }
    }
}