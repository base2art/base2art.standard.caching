﻿namespace Base2art.Caching
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    public static class Cache
    {
        public static CacheLookupWrapper<T> As<T>(this ICache<string, object> cache) => new CacheLookupWrapper<T>(cache);

        /* ****************** SYNC API ****************/
        public static TResult TryGet<TResult>(this ICache<string, object> cache, Func<TResult> getter) => cache.TryGet<object, TResult>(getter);

        public static TActualResult TryGet<TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, Func<TActualResult> getter)
            where TActualResult : TCacheValue => cache.TryGetGeneric(
                                                                     getter.GetMethodInfo(),
                                                                     new KeyValuePair<Type, object>[] { },
                                                                     getter);

        public static TResult TryGet<TIn1, TResult>(this ICache<string, object> cache, TIn1 in1, Func<TIn1, TResult> getter) =>
            cache.TryGet<TIn1, object, TResult>(in1, getter);

        public static TActualResult TryGet<TIn1, TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, TIn1 in1,
                                                                             Func<TIn1, TActualResult> getter)
            where TActualResult : TCacheValue
        {
            return cache.TryGetGeneric(
                                       getter.GetMethodInfo(),
                                       new[] {new KeyValuePair<Type, object>(typeof(TIn1), in1)},
                                       () => getter(in1));
        }

        /* ****************** ASYNC API ****************/
        public static Task<TResult> TryGetAsync<TResult>(this ICache<string, object> cache, Func<Task<TResult>> getter) =>
            cache.TryGetAsync<object, TResult>(getter);

        public static Task<TActualResult> TryGetAsync<TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache,
                                                                                  Func<Task<TActualResult>> getter)
            where TActualResult : TCacheValue => cache.TryGetGenericAsync(
                                                                          getter.GetMethodInfo(),
                                                                          new KeyValuePair<Type, object>[] { },
                                                                          getter);

        public static Task<TResult> TryGetAsync<TIn1, TResult>(this ICache<string, object> cache, TIn1 in1, Func<TIn1, Task<TResult>> getter)
            where TResult : class => cache.TryGetAsync<TIn1, object, TResult>(in1, getter);

        public static Task<TActualResult> TryGetAsync<TIn1, TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, TIn1 in1,
                                                                                        Func<TIn1, Task<TActualResult>> getter)
            where TActualResult : TCacheValue
        {
            return cache.TryGetGenericAsync(
                                            getter.GetMethodInfo(),
                                            new[] {new KeyValuePair<Type, object>(typeof(TIn1), in1)},
                                            () => getter(in1));
        }

        private static TActualResult TryGetGeneric<TCacheValue, TActualResult>(
            this ICache<string, TCacheValue> cache,
            MethodInfo method,
            KeyValuePair<Type, object>[] arguments,
            Func<TActualResult> callback)
            where TActualResult : TCacheValue
        {
            var key = BuildCacheKey(method, arguments);
            if (string.IsNullOrWhiteSpace(key))
            {
                return callback();
            }

            var item = cache.GetItem(key);
            if (item != null)
            {
                return (TActualResult) item;
            }

            var value = callback();
            if (value != null)
            {
                cache.Upsert(key, value);
            }

            return value;
        }

        private static async Task<TActualResult> TryGetGenericAsync<TCacheValue, TActualResult>(
            this ICache<string, TCacheValue> cache,
            MethodInfo method,
            KeyValuePair<Type, object>[] arguments,
            Func<Task<TActualResult>> callback)
            where TActualResult : TCacheValue
        {
            var key = BuildCacheKey(method, arguments);
            if (string.IsNullOrWhiteSpace(key))
            {
                return await callback();
            }

            var item = cache.GetItem(key);
            if (item != null)
            {
                return (TActualResult) item;
            }

            var value = await callback();
            if (value != null)
            {
                cache.Upsert(key, value);
            }

            return value;
        }

        private static string BuildCacheKey(MethodBase method, params KeyValuePair<Type, object>[] arguments)
        {
            var sb = new StringBuilder();
            sb.Append(method.DeclaringType.FullName);
            sb.Append(".");
            sb.Append(method.Name);
            sb.Append('(');

            const string separator = "^✓$✓^";

            for (var i = 0; i < arguments.Length; i++)
            {
                var argument = arguments[i];
                if (argument.Key != typeof(string))
                {
                    var typeInfo = argument.Key.GetTypeInfo();
                    if (!typeInfo.IsValueType && !typeInfo.IsEnum)
                    {
                        return null;
                    }
                }

                var argumentKey = argument.Value == null ? "__NULL__" : argument.ToString();
                if (argumentKey.Contains(separator))
                {
                    return null;
                }

                sb.Append(argumentKey);

                if (i >= 1)
                {
                    sb.Append(separator);
                }
            }

            sb.Append(')');

            return sb.ToString();
        }
    }
}